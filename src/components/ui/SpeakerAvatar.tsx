export interface SpeakerAvatarProps {
    speaker: string
}

export const SpeakerAvatar = ({speaker}: SpeakerAvatarProps) => {
    return <div style={{padding: 12, background: '#ccc', display: 'inline-block', textAlign: 'center'}}>
        <img style={{width: 150, height: 150, borderRadius: 150, overflow: 'hidden'}} src={"https://i.pravatar.cc/150?u="+speaker} /><br /> 
        {speaker}
    </div>
}