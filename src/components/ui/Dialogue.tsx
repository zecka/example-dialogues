import { DialogueAction, DialogueMessage } from "../common-types";
import { SpeakerAvatar } from "./SpeakerAvatar";

export interface DialogueProps {
    messages: DialogueMessage[]
    actions: DialogueAction[]
    onAction: (action: DialogueAction) => void
}


export const Dialogue = ({messages, actions, onAction}: DialogueProps) => {

    return <div>
        {messages.map(m=><div key={m.text}>
           <div><SpeakerAvatar speaker={m.speaker} /> {m.text}</div>
        </div>)}
        {actions.map(a=><button key={a.label} onClick={()=>onAction(a)}>{a.label}</button>)}
    </div>
}