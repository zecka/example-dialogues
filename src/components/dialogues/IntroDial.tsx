import { Dialogue} from "../ui/Dialogue";
import { useGameStore } from "../GameStore";
import { DialogueMessage, DialogueAction, SPEAKERS } from "../common-types";

const messages: DialogueMessage[] = [{
    speaker: SPEAKERS.NARRATOR,
    text: "Tu te promène dans une ruelle et tu croise Pierre et Paul",
},
{
    speaker: SPEAKERS.PAUL,
    text: "Bonjour inconnu",
},
{
    speaker: SPEAKERS.PIERRE,
    text: "Je ne vous dis pas bonjour, vous avez l'air idiot !",
}
]

const actions: DialogueAction[] = [{
    label: "Tuer Pierre",
    id: "KILL_PIERRE",
    target: "WALK_IN_CHINATOWN"
},
{
    label: "Continuer son chemin",
    id: "CONTINUE",
    target: "WALK_IN_FOREST"
}];

export const IntroDial = () => {
    const { setGameState, setCurrentScene } = useGameStore()

    const handleAction = (action: DialogueAction) => {
        if (action.id === 'KILL_PIERRE') {
            setGameState({ isPierreDead: true })
        }
        setCurrentScene(action.target)
    }

    return <div>
        <h1>Ruelle</h1>
        <Dialogue messages={messages} actions={actions} onAction={handleAction} />
    </div>
}