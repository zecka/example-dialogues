import { Dialogue} from "../ui/Dialogue"
import { useGameStore } from "../GameStore"
import { DialogueMessage, DialogueAction, SPEAKERS } from "../common-types"

export const WalkInChinatownDial = () => {
    const {isPierreDead, setCurrentScene, setGameState, cash} = useGameStore()

    const messages: DialogueMessage[] =[ {
        speaker: SPEAKERS.NARRATOR,
        text: "Tu te promène dans chinatown",
    }]

    const actions: DialogueAction[] = [];
    if(isPierreDead){
        messages.push({
            speaker: SPEAKERS.PAUL,
            text: "Tu as tué pierre, donne moi 10000$ ou j'appel la police"
        })
        actions.push({
            label: "Oui",
            id: "PAY",
            target: 'END'
        })
        actions.push({
            label: "NON",
            id: "DONT_PAY",
            target: 'END'
        })
    } else {
        messages.push({
            speaker: SPEAKERS.PIERRE,
            text: "Hello, comment vas-tu. Nous allons à brooklyn, veux tu nous suivre"
        })
        actions.push({
            label: "Oui",
            id: "YES",
            target: 'END'
        })
        actions.push({
            label: "NON",
            id: "NO",
            target: 'END'
        })
    }

    const handleAction = (action: DialogueAction) => {
        if(action.id === 'PAY'){
            setGameState({cash: cash - 1000})
        }else if (action.id === 'YES'){
            setGameState({acceptBrooklyn: true})
        }
        setCurrentScene(action.target)
    }


    return <div>
        <h1>CHINATOWN</h1>
        <Dialogue messages={messages} actions={actions} onAction={handleAction} />
    </div>
}