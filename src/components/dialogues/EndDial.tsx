import { useGameStore } from "../GameStore"

export const EndDial = () => {
    const {isPierreDead, cash, acceptBrooklyn} = useGameStore()
    return <div>
        <h1>FIN</h1>
        <div>Il vous reste {cash}$ </div>
        {!!isPierreDead && <div>Pierre est mort</div>}
        {!isPierreDead && acceptBrooklyn && <div>Vous êtes a Brooklyn avec Pierre et Paul</div>}
        {!isPierreDead && !acceptBrooklyn && <div>Pierre et Paul sont a brooklyn, vous êtes resté à chinatown</div>}

    </div>
}