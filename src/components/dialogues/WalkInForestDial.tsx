import { Dialogue } from "../ui/Dialogue"
import { basicHandleAction } from "../GameStore"
import { DialogueMessage, DialogueAction, SPEAKERS } from "../common-types"

export const WalkInForestDial = () => {
    const messages: DialogueMessage[] = [
        {
            text: "Les papillons volent, mais on se fait chier...",
            speaker: SPEAKERS.NARRATOR
        }
    ]

    const actions: DialogueAction[] = [
        {
            label: "Continuer",
            id: "CONTINUE",
            target: "WALK_IN_CHINATOWN"
        }
    ]



    return <div>
        <h1>Forest</h1>
        <Dialogue messages={messages} actions={actions} onAction={basicHandleAction}  />
        </div>

}