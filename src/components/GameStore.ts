import { create } from 'zustand'
import { SceneKey } from '../scenes'
import { DialogueAction } from './common-types';

interface GameStore {
    cash: number;
    currentScene: SceneKey;
    isPierreDead: boolean;
    acceptBrooklyn: boolean;
    setCurrentScene: (scene: SceneKey) => void
    setGameState: (state: Partial<GameStore>) => void
  }
  
export const useGameStore = create<GameStore>((set) => ({
  currentScene: 'INTRO',
  cash: 1000,
  isPierreDead: false,
  acceptBrooklyn: false,
  setCurrentScene: (key) => set({currentScene: key}),
  setGameState: (state) => set(state)
}))

export const basicHandleAction = (action:DialogueAction) => {
    useGameStore.setState({currentScene: action.target})
}