import { SceneKey } from "../scenes";

export interface DialogueAction {
    label: string;
    id:string;
    target: SceneKey
}
export interface DialogueMessage {
    speaker: string;
    text: string
}
export const SPEAKERS = {
    NARRATOR: "Narrateur",
    PIERRE: "Pierre",
    PAUL: "Paul",
}
export type SpeakerKey = keyof typeof SPEAKERS;
