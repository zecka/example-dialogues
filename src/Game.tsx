import { useGameStore } from "./components/GameStore"
import { Scenes } from "./scenes"

export const Game = () => {
    const { currentScene } = useGameStore()
    const Current = Scenes[currentScene]
    return <div>
        <Current />
    </div>
}