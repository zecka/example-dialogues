import { EndDial } from "./components/dialogues/EndDial"
import { IntroDial } from "./components/dialogues/IntroDial"
import { WalkInChinatownDial } from "./components/dialogues/WalkInChinatownDial"
import { WalkInForestDial } from "./components/dialogues/WalkInForestDial"

export const Scenes = {
    INTRO: IntroDial,
    WALK_IN_CHINATOWN: WalkInChinatownDial,
    WALK_IN_FOREST: WalkInForestDial,
    END: EndDial
}

export type SceneKey = keyof typeof Scenes